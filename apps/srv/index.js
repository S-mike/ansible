'use strict';

const http = require('http');

console.log('Starting server');

http.createServer((request, response) => {
  request.on('error', (err) => {
    console.error(err);
    response.statusCode = 400;
    response.end();
  });
  response.on('error', (err) => {
    console.error(err);
  });

  const commonTags = {
    host: 'localhost',
    region: 'us-west',
    dc: 'san-jose',
  }

  const r = {
    ...commonTags
  };

  if (request.method === 'GET' && request.url === '/echo') {
    const metrics = JSON.stringify([
      {
        ...r,
        fileCount: parseInt(Date.now().toString().slice(-5, -3)),
        src: 'http'
      },
      {
        ...r,
        fileCount: 1,
        type: 'FLOWSTATS',
        src: 'sum'
      },
      {
        ...r,
        fileCount: 2,
        type: 'LINKSTATS',
        src: 'sum'
      }
    ]);
    console.log(new Date(), metrics);

    response.end(metrics);
  }
  else {
    console.log(request.method, request.url);
    //response.statusCode = 404;
    response.end('Hello');
  }
}).listen(8777);
