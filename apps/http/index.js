'use strict';

const http = require('http');

console.log('Starting server');

http.createServer((request, response) => {
  request.on('error', (err) => {
    console.error(err);
    response.statusCode = 400;
    response.end();
  });
  response.on('error', (err) => {
    console.error(err);
  });



  if (request.method === 'GET' && request.url === '/echo') {

    const metric = JSON.stringify(getMetric());
    console.log(new Date(), metric);
    response.end(metric);
  }
  else {
    console.log(request.method, request.url);
    //response.statusCode = 404;
    response.end('Hello');
  }
}).listen(8777);

function getHost() {
  const hosts = [
    'localhost',
    'london',
    'new-york',
    'paris',
    'rome'
  ];

  return hosts[Math.floor(Math.random() * (hosts.length - 0)) + 0];
}

const commonTags = {
  region: 'us-west',
  zone: 'san-jose',
  src: 'http'
}

function getMetric(){
  const metrics = [
    {
      fileCount: 1,
      type: 'FLOWSTATS',
      ...commonTags
    },
    {
      fileCount: 2,
      type: 'LINKSTATS',
      ...commonTags
    }
  ];

  let host = getHost();

  metrics.forEach(e => {
    e.host = host;
  });

  // return metrics[Math.floor(Math.random() * (metrics.length - 0)) + 0];
  return metrics;
}
