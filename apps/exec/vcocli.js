#!/usr/bin/env node

'use strict';

function rand(type){
	switch(type){
 		case 'FLOWSTATS':
			return parseInt(Date.now().toString().slice(-5, -3));
			break;
		case 'LINKSTATS':
			return -1*parseInt(Date.now().toString().slice(-5, -3));
			break;
		default:
			return 0;
			break;
	}
}

console.log(JSON.stringify([
	{
		fileCount: rand('FLOWSTATS'),
		type: 'FLOWSTATS',
		host: 'abs',
		region: 'us-west',
		dc: 'san-jose'
	},
        {
		fileCount: rand('LINKSTATS'),
                type: 'LINKSTATS',
                host: 'abs',
                region: 'us-west',
                dc: 'san-jose'
        }
]));

