'use strict';

const cluster = require('cluster');
const http = require('http');
const StatsD = require('hot-shots');
const numCPUs = require('os').cpus().length;
var counter = 0;

const clusterLabel = 'cluster01';
const statsdConf = {telegraf: true};
if(process.env.statsdHost !== undefined){
  statsdConf.host = process.env.statsdHost;
}

if (cluster.isMaster) {
  console.log(`Cluster starting... CPU's: ${numCPUs}, statsD host: ${statsdConf.host}`);

  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  for (const id in cluster.workers) {
    cluster.workers[id].on('message', (msg) => {
      counter++;
    });
  }

  setInterval(() => {
    console.log(`${new Date()}: Amount of metrics was send: ${counter}`);
  }, 10000)

} else {
  const client = new StatsD(statsdConf);
  const timerPeriod = Math.floor(Math.random() * (3000 - 500)) + 500;
  const gaugePeriod = Math.floor(Math.random() * (10000 - 5000)) + 5000;

  console.log(`
Worker ${process.pid} started
Timer period  is ${timerPeriod} ms
Gauge and counter period is ${gaugePeriod}`);

  const timers = setInterval(() => {
    client.timing('timer', generateMetrict(10, 1000), generateTags());
    process.send({});
  }, timerPeriod);

  const gauges = setInterval(() => {
    client.gauge(
      'gaugeMetric',
      generateSaw(),
      generateTags()
    );
    process.send({});
  }, gaugePeriod);

  const counter = setInterval(() => {
    client.increment('counterMetric', generateTags());
    process.send({});
  }, gaugePeriod);
}

function generateMetrict(min, max, koef = 1){
  return (Math.floor(Math.random() * (max - min)) + min)*koef;
}

function generateSaw(offset = 0, koef = 1){
  return (offset + parseInt(Date.now().toString().slice(-5, -3)))*koef;
}

function generateTags(){
  return {
    host: getHost(),
    region: 'us-west',
    zone: 'san-jose',
    src: 'statsd'
  }
}

function getHost() {
  const hosts = [
    'localhost',
    'london',
    'new-york',
    'paris',
    'rome'
  ];
  return hosts[Math.floor(Math.random() * (hosts.length - 0)) + 0]
}
