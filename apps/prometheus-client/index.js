'use strict';

const express = require('express');
const cluster = require('cluster');
const server = express();
const register = require('prom-client').register;

const getLabelNames = () => ["host", "region", "zone", "src"];

function getHost() {
  const hosts = [
    'localhost',
    'london',
    'new-york',
    'paris',
    'rome'
  ];

  return hosts[Math.floor(Math.random() * (hosts.length - 0)) + 0];
}

const getLabelValues = () => [getHost(), 'us-west', 'san-jose', 'prom-client'];

const commonTags = () => {
  return {
    host: getHost(),
    region: 'us-west',
    zone: 'san-jose',
    src: 'prom-client'
  }
}

const Histogram = require('prom-client').Histogram;
const h = new Histogram({
	name: 'test_histogram',
	help: 'Example of a histogram',
	labelNames: getLabelNames()
});

const Counter = require('prom-client').Counter;
const c = new Counter({
	name: 'test_counter',
	help: 'Example of a counter',
	labelNames: getLabelNames()
});

const Gauge = require('prom-client').Gauge;
const g = new Gauge({
	name: 'test_gauge',
	help: 'Example of a gauge',
	labelNames: getLabelNames()
});

setInterval(() => {
	h.labels(...getLabelValues()).observe(Math.random());
	// h.labels(...getLabelValues()).observe(Math.random());
}, 10000);

setInterval(() => {
	c.labels(...getLabelValues()).inc();
}, 2000);

setInterval(() => {
	g.set(commonTags(), Math.random());
}, 100);


server.get('/metrics', (req, res) => {
	res.set('Content-Type', register.contentType);
	res.end(register.metrics());
});

server.get('/metrics/counter', (req, res) => {
	res.set('Content-Type', register.contentType);
	res.end(register.getSingleMetricAsString('test_counter'));
});

//Enable collection of default metrics
// require('prom-client').collectDefaultMetrics();

console.log('Server listening to 8778, metrics exposed on /metrics endpoint');
server.listen(8778);
