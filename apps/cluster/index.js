'use strict';

const cluster = require('cluster');
const http = require('http');
let numCPUs = require('os').cpus().length;
//numCPUs =
var counter = 0;

const clusterLabel = 'cluster01';
const statsdConf = {telegraf: true};
if(process.env.statsdHost !== undefined){
  statsdConf.host = process.env.statsdHost;
}

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);
  const StatsD = require('hot-shots');
  const client = new StatsD(statsdConf);

  client.gauge(
      'clusterStart',
      generateMetrict(0, 0),
      generateTags()
    );

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }


  for (const id in cluster.workers) {
    cluster.workers[id].on('message', (msg) => {
      console.log(`${new Date()} master: `);
      console.log(`   ->  worker with id ${id} had sent message: ${JSON.stringify(msg)}`);
      console.log(`   -> counter is ${counter}`)
      counter++;
      console.log('');

      if(msg.action){
        switch(msg.action){
	  case 1:
            client.gauge(
      	      msg.msg.metricName,
              msg.msg.metricValue,
      	      msg.msg.tags
       	    );
	    break;
	  default:
	    console.log(`ERROR! No action with id ${msg.action}`);
            break;
	}
      }
    });
  }


  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  const StatsD = require('hot-shots');
  const client = new StatsD(statsdConf);

  setInterval(() => {
    console.log(`I am worker #${cluster.worker.id}`);
    client.gauge(
      'fileCount',
      generateMetrict(cluster.worker.id*100, cluster.worker.id*100+50),
      generateTags('LINKSTATS', cluster.worker.id)
    );

    client.gauge(
      'fileCount',
      generateMetrict(cluster.worker.id*100+500, cluster.worker.id*100+550),
      generateTags('FLOWSTATS', cluster.worker.id)
    );

    client.gauge(
      'fileCount',
      generateSaw(2000),
      generateTags('FLOWSTATS', 'allThreads')
    );

    client.increment('my_counter', generateTags('COUNTER', 'allThreads'));

    client.timing('response_time', generateMetrict(0, 100), generateTags('TIME', 'allThreads'));
    client.histogram('mike_histogram', generateMetrict(0, 100), generateTags('HISTOGRAM', 'allThreads'))

    process.send({
      action: 1,
      msg: {
        metricName: 'fileCount',
        metricValue: generateMetrict(1000, 1100),
        tags: generateTags('LINKSTATS', 'master')
      }
    });
  }, Math.floor(Math.random() * (10000 - 5000)) + 5000)
  console.log(`Worker ${process.pid} started`);
}

function generateMetrict(min, max, koef = 1){
  return (Math.floor(Math.random() * (max - min)) + min)*koef;
}

function generateSaw(offset = 0, koef = 1){
  return (offset + parseInt(Date.now().toString().slice(-5, -3)))*koef;
}

function generateTags(type = 'LINKSTATS', workerId = 'untaged'){
  return {
    type,
    host: getHost(),
    region: 'us-west',
    dc: 'san-jose',
    cluster: clusterLabel,
    workerId
  }
}

function getHost() {
  const hosts = [
    'localhost',
    'london',
    'new-york',
    'paris',
    'rome'
  ];
  return hosts[Math.floor(Math.random() * (hosts.length - 0)) + 0]
}
