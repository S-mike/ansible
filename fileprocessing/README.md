# Params to change

- `prometheus/prometheus.yml` scrapes configs
  - `job_name: 'telegraf'` -> `targets` replace `telegraf_ip` to Telegraf IP server
  - `job_name: 'promclient'` -> `targets` replace `vco_fileprocessing_metrics_ip` to VCO File Processing Server IP server