`docker network create -d bridge obser01`

`docker run -d \
    --name graphite \
    --net obser01 \
    --restart=always \
    -p 80:80 \
    -p 2003-2004:2003-2004 \
    -p 2023-2024:2023-2024 \
    -p 8126:8126 \
    -v /Users/michael/Work/repos/ansible/configs/docker/carbon.conf.docker:/opt/graphite/conf/carbon.conf:ro \
    -v /Users/michael/Work/repos/ansible/configs/docker/volumes/graphite:/opt/graphite/storage/ \
    graphiteapp/graphite-statsd`


`docker run -d \
    --name=telegraf \
    --net obser01 \
    -p 8125:8125/udp \
    -v /Users/michael/Work/repos/ansible/configs/docker/telegraf.conf.docker:/etc/telegraf/telegraf.conf:ro \
    telegraf`


`docker run -d \
    --name=grafana \
    --net obser01 \
    -p 3000:3000 \
    -v /Users/michael/Work/repos/ansible/configs/docker/grafana.provision.datasource.yaml:/etc/grafana/provisioning/datasources/datasource.yaml:ro \
    -v /Users/michael/Work/repos/ansible/configs/docker/grafana.provision.dashboard.yaml:/etc/grafana/provisioning/dashboards/dashboard.yaml:ro \
    -v /Users/michael/Work/repos/ansible/configs/docker/grafana.panels.dashboard.yaml:/var/lib/grafana/dashboards/origin.yaml \
    -v /Users/michael/Work/repos/ansible/configs/docker/volumes/grafana:/var/lib/grafana \
    grafana/grafana`

`docker run -d \
    --name prometheus \
    --net obser01 \
    -p 9090:9090 \
    -p 9273:9273 \
    -v /Users/michael/Work/repos/ansible/configs/docker/prometheus.yml:/etc/prometheus/prometheus.yml:ro \
    -v /Users/michael/Work/repos/ansible/configs/docker/volumes/prometheus:/prometheus \
    quay.io/prometheus/prometheus`


or

`sudo docker-compose up`

`sudo chown -R 472:472 ${git}/configs/docker/volumes/grafana/`